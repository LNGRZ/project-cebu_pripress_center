<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pripress
 */
 
?>

<div id="secondary" class="site-sidebar">
	<aside id="aside" class="site-aside">
		<div id="<?php if ( is_archive() ) { echo 'side-focus-2'; } else { echo 'side-focus-3'; } ?>" class="sidebar-focus">
			<div class="feat-aritcles">
				<?php
				if ( is_category(13) ) : ?>
				<h5 class="main-title">注目の作品</h5>
				<?php 
				else : ?>
				<h5 class="main-title">Featured Works</h5>
				<?php
				endif; ?>
				<div class="card">
					<div class="card-content">
					<?php 
					$the_query = new WP_Query( 'posts_per_page=5&cat=23' );
					while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
						<div class="feat-entry-cluster row">
							<div class="feat-img-s">
								<?php 
								if ( has_post_thumbnail() ) : ?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('small'); ?></a>
								<?php 
								else : ?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
									<div class="feat-img-pholder-s">
										<img src="<?php bloginfo('stylesheet_directory')?>/assets/logo-cebu_pripress-dark.png" />
									</div>
								</a>
								<?php 
								endif; ?>
							</div>
							<div class="feat-entry-s">
								<p class="title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></p>
							</div>
						</div>
					<?php 
					endwhile;
					wp_reset_postdata();
					?>	
					</div>
				</div>
			</div>
		</div>
	</aside>
</div><!-- #secondary -->