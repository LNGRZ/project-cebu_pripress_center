<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pripress
 */
 
?>

<div id="secondary" class="site-sidebar">
	<aside id="aside" class="site-aside">
	<?php 
	if ( is_home() ) : 
		include( 'sidebar-a.php' ); ?>
		<div id="side-focus-1" class="sidebar-focus">
		<?php include( 'sidebar-c.php' ); ?>
		</div>

	<?php
	elseif ( is_archive() ) :
		include( 'sidebar-b.php' ); ?>
		<div id="side-focus-1" class="sidebar-focus">
		<?php include( 'sidebar-c.php' ); ?>
		</div>
		
	<?php 
	else : 
		include( 'sidebar-a.php' );
		include( 'sidebar-b.php' );
		include( 'sidebar-c.php' );
	endif 
	?>
	</aside>
</div><!-- #secondary -->