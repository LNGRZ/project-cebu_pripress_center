<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pripress
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div class="title-bar">
	<h2 class="title">
	<?php 
	if ( is_page_template('single-japanese.php') ) :
		echo 'コメント';
	else :
		echo 'Comments';
	endif; ?>
	</h2>
</div>
<div class="section">
	<div class="card">
		<div class="card-content">
			<div id="comments" class="comments-area">

				<?php
				// You can start editing here -- including this comment!
				if ( have_comments() ) : ?>

					<?php the_comments_navigation(); ?>

					<ol class="comment-list">
						<?php
							wp_list_comments( array(
								'style'      => 'ul',
								'short_ping' => true,
							) );
						?>
					</ol><!-- .comment-list -->

					<?php the_comments_navigation();

					// If comments are closed and there are comments, let's leave a little note, shall we?
					if ( ! comments_open() ) : ?>
						<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'pripress' ); ?></p>
					<?php
					endif;

				endif; // Check for have_comments().

				comment_form();
				?>

			</div><!-- #comments -->
		</div>
	</div>
</div>
