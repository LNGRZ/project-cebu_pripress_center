<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pripress
 */

get_header('jp'); ?>

	<?php get_template_part( 'template-parts/content', 'atf' ); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section>

				<ul class="tabs tabs-fixed-width">
					<li class="tab col s4"><a href="#tab1">情報</a></li>
					<li class="tab col s4"><a href="#tab2">デモ</a></li>
				</ul>

				<div id="tab1">

					<div class="title-bar">
						<h2 class="title"><?php echo get_the_title(34); ?></h2>
					</div>
					<div class="section">
						<div class="card row">
							<div class="col s12 l4">
								<div class="card-content al-center">
									<?php
									$post_id = 93;
									$queried_post = get_post($post_id);
									$title = $queried_post->post_title;
									?>
									<div class="prod_serv-logo red"><i class="font-icons">&#xE3B7;</i></div>
									<h3 class="title"><?php echo $title;?></h3>
									<p class="entry al-justify "><?php echo $queried_post->post_content; ?></p>
								</div>
							</div>
							<div class="col s12 l4">
								<div class="card-content al-center">
									<?php
									$post_id = 95;
									$queried_post = get_post($post_id);
									$title = $queried_post->post_title;
									?>
									<div class="prod_serv-logo blue"><i class="font-icons">&#xE86F;</i></div>
									<h3 class="title"><?php echo $title;?></h3>
									<p class="entry al-justify "><?php echo $queried_post->post_content; ?></p>
								</div>
							</div>
							<div class="col s12 l4">
								<div class="card-content al-center">
									<?php
									$post_id = 97;
									$queried_post = get_post($post_id);
									$title = $queried_post->post_title;
									?>
									<div class="prod_serv-logo orange"><i class="font-icons">&#xE80D;</i></div>
									<h3 class="title"><?php echo $title;?></h3>
									<p class="entry al-justify "><?php echo $queried_post->post_content; ?></p>
								</div>
							</div>
						</div>
					</div>

					<div class="title-bar">
						<h2 class="title">ショートクリップ</h2>
					</div>
					<div class="short-clip">
						<div classs="section">
							<p class="no-vid">現時点でビデオはありません...</p>
						</div>
					</div>

					<div class="title-bar">
						<?php
						$post_id = 227;
						$queried_post = get_post($post_id);
						$title = $queried_post->post_title;
						?>
						<h2 class="title"><?php echo $title;?></h2>
					</div>
					<div class="section">
						<div class="card">
							<div class="card-content">
								<p class="entry al-justify "><?php echo $queried_post->post_content; ?></p>
							</div>
						</div>
					</div>

					<div class="title-bar">
						<?php
						$post_id = 225;
						$queried_post = get_post($post_id);
						$title = $queried_post->post_title;
						?>
						<h2 class="title"><?php echo $title;?></h2>
					</div>
					<div class="section">
						<div class="card">
							<div class="card-content">
								<p class="entry al-justify "><?php echo $queried_post->post_content; ?></p>
							</div>
						</div>
					</div>

					<div class="title-bar">
						<h2 class="title"><?php echo get_cat_name(9); ?></h2>
						<a class="waves-effect waves-light btn btn-desktop" href="<?php echo esc_url( get_category_link(9) ); ?>">View All</a>
					</div>
					<?php get_template_part( 'template-parts/content', 'works' ); ?>
					<a class="waves-effect waves-light btn btn-mobile" href="<?php echo esc_url( get_permalink(14) ); ?>">View All</a>

				</div>

				<div id="tab2">
					<div class="title-bar">
						<h2 class="title">デモをリクエストする</h2>
					</div>
					<div class="section">
						<div class="card">
							<div class="card-content">
							<?php
							while ( have_posts() ) : the_post();

								get_template_part( 'template-parts/content', 'page' );

								// If comments are open or we have at least one comment, load up the comment template.
								if ( comments_open() || get_comments_number() ) :
									comments_template();
								endif;

							endwhile; // End of the loop.
							?>
							</div>
						</div>
					</div>
				</div>

			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer('jp');
