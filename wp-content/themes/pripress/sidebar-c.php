<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pripress
 */
 
?>

			<div class="categories-list">
				<h5 class="main-title">Category</h5>
				<div class="card">
					<ul class="card-content">
						<?php wp_list_categories( array(
							'orderby'    => 'name',
							'title_li' => '',
							'show_count' => true,
							'include'    => array( 1, 2, 3, 10, 11, 12 )
						) ); ?> 
					</ul>
				</div>
			</div>
			<div class="widget-area">
				<?php dynamic_sidebar( 'sidebar-1' ); ?>
			</div>
