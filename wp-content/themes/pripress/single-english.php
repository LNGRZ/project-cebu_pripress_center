<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * Template Name: English Article
 * Template Post Type: post
 *
 * @package pripress
 */

get_header(); ?>

	<?php get_template_part( 'template-parts/content', 'atf' ); ?>

	<div id="primary" class="content-area has-sidebar">
		<main id="main" class="site-main">
			<?php
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content', get_post_type() );
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
