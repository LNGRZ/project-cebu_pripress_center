<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pripress
 */

get_header('jp'); ?>

	<!-- ATF/HEADLINE -->
	<?php get_template_part( 'template-parts/content', 'atf' ); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section>
				<!-- PRODUCT AND SERVICES SECTION -->
				<div class="title-bar">
					<h2 class="title"><?php echo get_the_title(34); ?></h2>
				</div>
				<div class="section no-pad">
					<div class="card">
						<div class="card-content row">
							<div class="col s12 l4">
								<a class="prod_serv-bundle" href="<?php echo esc_url( get_permalink(34) ); ?>">
									<?php
									$post_id = 93;
									$queried_post = get_post($post_id);
									$title = $queried_post->post_title;
									?>
									<div class="prod_serv-logo red"><i class="font-icons">&#xE3B7;</i></div>
									<h3 class="title"><?php echo $title;?></h3>
									<p class="entry"><?php echo $queried_post->post_content; ?></p>
								</a>
							</div>
							<div class="col s12 l4">
								<a class="prod_serv-bundle" href="<?php echo esc_url( get_permalink(34) ); ?>">
									<?php
									$post_id = 95;
									$queried_post = get_post($post_id);
									$title = $queried_post->post_title;
									?>
									<div class="prod_serv-logo blue"><i class="font-icons">&#xE86F;</i></div>
									<h3 class="title"><?php echo $title;?></h3>
									<p class="entry"><?php echo $queried_post->post_content; ?></p>
								</a>
							</div>
							<div class="col s12 l4">
								<a class="prod_serv-bundle" href="<?php echo esc_url( get_permalink(34) ); ?>">
									<?php
									$post_id = 97;
									$queried_post = get_post($post_id);
									$title = $queried_post->post_title;
									?>
									<div class="prod_serv-logo orange"><i class="font-icons">&#xE80D;</i></div>
									<h3 class="title"><?php echo $title;?></h3>
									<p class="entry"><?php echo $queried_post->post_content; ?></p>
								</a>
							</div>
						</div>
						<div class="card-content">
							<a class="waves-effect waves-light btn-large grey-800 btn-desktop" href="<?php echo esc_url( get_permalink(37) . '#tab2' ); ?>">ビジネスアシスタンスを受ける</a>
						</div>
					</div>
					<a class="waves-effect waves-light btn btn-mobile" href="<?php echo esc_url( get_permalink(37) . '#tab2' ); ?>">ビジネスアシスタンスを受ける</a>
				</div>
				<!-- ARTICLE SECTION -->
				<div class="title-bar">
					<h2 class="title"><?php echo get_the_title(124); ?></h2>
					<a class="waves-effect waves-light btn btn-desktop" href="<?php echo esc_url( get_permalink(124) ); ?>">もっと見る</a>
				</div>
				<div class="section row">
					<div class="article-featured main">
					<?php 
					$the_query = new WP_Query( array ('posts_per_page' => 1, 'cat' => 7 ) );
					while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
						<div class="card">
							<div class="card-content row">
								<div class="featured-image col s12 l6">
									<?php 
									if ( has_post_thumbnail() ) : ?>
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?></a>
									<?php 
									else : ?>
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
										<div class="feat-img-pholder">
											<img src="<?php bloginfo('stylesheet_directory')?>/assets/logo-cebu_pripress-dark.png" />
										</div>
									</a>
									<?php 
									endif; ?>
								</div>
								<div class="feat-article-entry col s12 l6">
									<div class="date-entry">
										<span class="month"><?php echo get_the_date('F'); ?></span>
										<span class="day"><?php echo get_the_date('d'); ?></span>
									</div>
									<div class="article-title">
										<header>
											<h3 class="title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
											<span class="full-date"><?php echo get_the_date('D - F d, Y'); ?></span>
										</header>
									</div>
									<div class="article-entry">
										<?php echo get_excerpt(275) ?>
									</div>
									<div class="article-more card-action">
										<a href="<?php the_permalink() ?>">続きを読む...</a>
									</div>
								</div>
							</div>
						</div>
					<?php 
					endwhile;
					wp_reset_postdata();
					?>
					</div>
					<div class="article-featured">
					<?php 
					$the_query = new WP_Query( array ('posts_per_page' => 4, 'cat' => array('20') ) );
					while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
						<div class="card">
							<div class="card-content">
								<div class="date-entry">
									<span class="month"><?php echo get_the_date('F'); ?></span>
									<span class="day"><?php echo get_the_date('d'); ?></span>
								</div>
								<h3 class="title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
								<?php the_excerpt(); ?>
							</div>
						</div>
					<?php 
					endwhile;
					wp_reset_postdata();
					?>
					</div>
				</div>
				<a class="waves-effect waves-light btn btn-mobile" href="<?php echo esc_url( get_permalink(6) ); ?>">もっと見る</a>
				<!-- WORKS SECTION -->
				<div class="title-bar">
					<h2 class="title"><?php echo get_cat_name(13); ?></h2>
					<a class="waves-effect waves-light btn btn-desktop" href="<?php echo esc_url( get_category_link(13) ); ?>">もっと見る</a>
				</div>
				<?php get_template_part( 'template-parts/content', 'works' ); ?>
				<a class="waves-effect waves-light btn btn-mobile" href="<?php echo esc_url( get_category_link(13) ); ?>">もっと見る</a>
				<!-- ABOUT SECTION -->
				<div class="title-bar">
					<h2 class="title"><?php echo get_the_title(39); ?></h2>
					<a class="waves-effect waves-light btn btn-desktop" href="<?php echo esc_url( get_permalink(39) ); ?>">もっと見る</a>
				</div>
				<div class="section row">
					<div class="about-company-f">
						<div class="card background blue">
							<div class="card-content">
								<?php
								$post_id = 89;
								$queried_post = get_post($post_id);
								$title = $queried_post->post_title;
								?>
								<h3 class="title"><?php echo $title;?></h3>
								<img src="<?=bloginfo('stylesheet_directory')?>/assets/logo-cebu_pripress-light.png" />
								<p class="entry"><?php echo $queried_post->post_content; ?></p>
							</div>
						</div>
					</div>
					<div class="about-staff-f background white">
					<?php get_template_part( 'template-parts/content', 'staffs' ); ?>
					</div>
				</div>
				<a class="waves-effect waves-light btn btn-mobile" href="<?php echo esc_url( get_permalink(39) ); ?>">もっと見る</a>
				<!-- CONTACT SECTION -->
				<div class="title-bar">
					<h2 class="title"><?php echo get_the_title(37); ?></h2>
				</div>
				<div class="section row">
					<div class="contact-info">
						<div class="card background red">
							<div class="c_info-cluster">
								<i class="font-icons left">&#xE0CD;</i><p><?php echo get_post_meta($post->ID, 'PNumber', true); ?></p>
								<i class="font-icons left">&#xE158;</i><p><?php echo get_post_meta($post->ID, 'Email', true); ?></p>
								<i class="font-icons left">&#xE192;</i><p><?php echo get_post_meta($post->ID, 'Sched', true); ?></p>
								<i class="font-icons left">&#xE0C8;</i><p><?php echo get_post_meta($post->ID, 'Address', true); ?></p>
								<a class="waves-effect waves-light btn white" href="<?php echo esc_url( get_permalink(37) . '#tab2' ); ?>">メッセージを残す</a>
							</div>
						</div>
					</div>
					<div class="location-gmap background white">
					<?php get_template_part( 'template-parts/content', 'map' ); ?>
					</div>
				</div>
				
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer('jp');
