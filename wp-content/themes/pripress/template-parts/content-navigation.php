<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pripress
 */

?>

<div id="mobile-nav" class="site-navigation-m">
	<div class="site-branding-nav">
	<?php 
	if ( has_custom_logo() ): the_custom_logo(); else : ?>
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/logo-cebu_pripress_center.png"></a>
	<?php 
	endif; ?>			
	</div><!-- .site-branding-nav -->

	<div class="menu-content">
	<?php
	wp_nav_menu( array(
		'theme_location' => 'menu-1',
		'menu_id'        => 'english-menu',
	) );
	?>
	</div>

	<div class="menu-content">
		<ul>
			<li><a href="<?php echo esc_url( get_permalink(28) ); ?>">Japanese Version</a></li>
			<li><a href="<?php echo esc_url( get_permalink(8) . '#tab3' ); ?>">Partner Companies</a></li>
		</ul>
	</div>
</div>
