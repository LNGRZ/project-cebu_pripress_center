<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pripress
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="section">
		<div class="card">
			<div class="card-content">
	
				<header class="entry-header">
				<?php
				if ( is_singular() ) :
					the_title( '<h1 class="entry-title">', '</h1>' );
				else :
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				endif;

				if ( 'post' === get_post_type() ) : ?>
				<div class="entry-meta">
					<?php pripress_worked_on(); ?>
				</div><!-- .entry-meta -->
				<?php
				endif; ?>
				</header><!-- .entry-header -->


				<div class="entry-content">
				<?php
				the_content( sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'pripress' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				) );

				?>
				</div><!-- .entry-content -->
				
			</div>
		</div>
	</div>

</article><!-- #post-<?php the_ID(); ?> -->
