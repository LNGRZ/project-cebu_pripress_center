<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pripress
 */

?>

<div class="staff-cluster">
	<h3 class="title">
	<?php 
	if ( is_page(28) ) :
		echo '職員';
	else :
		echo 'Staff';
	endif ?>
	</h3>
	<div class="cluster row">
	<?php
	$blogusers = get_users( 'blog_id=1&orderby=post_count&role__not_in=Administrator&number=12=' );
	foreach ( $blogusers as $user ) {
	if ( $user ) : ?>
		<div class="staff-entry">
			<img src="<?php echo esc_url( get_avatar_url( $user->ID ) ); ?>" />
			<span class="name"><?php echo esc_html( $user->display_name ); ?></span>
			<span class="job"><?php echo esc_html( $user->jobtitle ); ?></span>
		</div>
	<?php 
	endif; } ?>
	</div>
</div>


