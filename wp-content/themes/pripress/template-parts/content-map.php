<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pripress
 */

?>

						<div id="map" class="map"></div>
						<script>
						function initMap() {
							var uluru = {lat: 10.342604, lng: 123.9182708};
							var map = new google.maps.Map(document.getElementById('map'), {
							zoom: 19,
							center: uluru
							});
							var marker = new google.maps.Marker({
							position: uluru,
							map: map
							});
						}
						</script>
						<script async defer
						src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZc9RjSCmm4fWWZ-q4hlKEqmxOlMFzAUg&callback=initMap">
						</script>
