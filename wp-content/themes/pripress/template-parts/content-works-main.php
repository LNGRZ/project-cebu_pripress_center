<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pripress
 */

?>

<div class="section">
	<article id="post-<?php the_ID(); ?>" <?php post_class('col s12 m6 xl4'); ?>>
			<div class="article-entry main">
				<div class="card">
					<div class="card-image">
						<?php 
						if ( has_post_thumbnail() ) : ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?></a>
						<?php 
						else : ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							<div class="feat-img-pholder">
								<img src="<?php bloginfo('stylesheet_directory')?>/assets/logo-cebu_pripress-dark.png" />
							</div>
						</a>
						<?php 
						endif; ?>
					</div>
					<div class="card-content">
						<header class="entry-header">
							<?php
							if ( is_singular() ) :
								the_title( '<h1 class="entry-title">', '</h1>' );
							else :
								the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
							endif;

							if ( 'post' === get_post_type() ) : ?>
							<div class="entry-meta">
								<?php pripress_worked_on(); ?>
							</div><!-- .entry-meta -->
							<?php
							endif; ?>
						</header><!-- .entry-header -->
					</div>
					<div class="card-action">
						<?php 
						if ( is_category(13) ) :?>
							<a href="<?php the_permalink() ?>">詳細を見る...</a>
						<?php 
						else :?>
							<a href="<?php the_permalink() ?>">View Details...</a>
						<?php 
						endif; ?>
					</div>
				</div>
			</div>
	</article><!-- #post-<?php the_ID(); ?> -->
</div>
