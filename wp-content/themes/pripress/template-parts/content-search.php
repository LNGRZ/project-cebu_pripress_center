<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pripress
 */

?>

<div class="section">
	<article id="post-<?php the_ID(); ?>" <?php post_class('col s12 m6'); ?>>
		<div class="article-entry main">
			<div class="card">
				<div class="card-image">
					<?php 
					if ( has_post_thumbnail() ) : ?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?></a>
					<?php 
					else : ?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
						<div class="feat-img-pholder">
							<img src="<?php bloginfo('stylesheet_directory')?>/assets/logo-cebu_pripress-dark.png" />
						</div>
					</a>
					<?php 
					endif; ?>
				</div>
				<div class="card-content">
					<header class="entry-header">
						<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

						<?php if ( 'post' === get_post_type() ) : ?>
						<div class="entry-meta">
							<?php pripress_posted_on(); ?>
						</div><!-- .entry-meta -->
						<?php endif; ?>
					</header><!-- .entry-header -->

					<div class="entry-summary">
						<?php echo get_excerpt(250); ?>
					</div><!-- .entry-summary -->
				</div>
				<div class="card-action">
				<?php 
				if ( is_category(20) ) :?>
					<a href="<?php the_permalink() ?>">もっと見る...</a>
				<?php 
				else :?>
					<a href="<?php the_permalink() ?>">Read More...</a>
				<?php 
				endif; ?>
				</div>
			</div>
		</div>
	</article><!-- #post-<?php the_ID(); ?> -->
</div>
