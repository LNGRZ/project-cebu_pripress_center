<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pripress
 */

?>

<?php
if ( is_front_page() || is_page(28) ) : ?>
    <div class="site-atf">
        <div class="atf-tagline">
            <?php 
            $description = get_bloginfo( 'description', 'display' );
            if ( $description || is_customize_preview() ) : ?>
                <h2 class="tagline"><?php echo $description; ?></h2>
            <?php
            endif;
            while ( have_posts() ) : the_post();
                get_template_part( 'template-parts/content', 'atf-info' );
            endwhile; // End of the loop.
            if ( is_front_page() ) : ?>
                <a class="waves-effect waves-light btn-large" href="<?php echo esc_url( get_permalink(12) ); ?>">Our <?php echo get_the_title(12); ?></a>
            <?php 
            else : ?>
                <a class="waves-effect waves-light btn-large" href="<?php echo esc_url( get_permalink(34) ); ?>">私達の<?php echo get_the_title(34); ?></a>
            <?php
            endif;
            ?>
        </div>
    </div>
<?php  
elseif ( is_single() ) : ?>
    <div class="feat-post-img parallax-container">
        <div class="parallax">
        <?php
        $page_id = get_queried_object_id();
        if ( has_post_thumbnail( $page_id ) ) :
            $image_array = wp_get_attachment_image_src( get_post_thumbnail_id( $page_id ), 'full' );
            $image = $image_array[0];
            $style_image = get_post_meta($post->ID, "stylefeatimage", true);
        else :
            $image = get_template_directory_uri() . '/assets/default-background.png';
        endif;
        echo '<img style="' . $style_image . ';" src="' . $image . '">';
        ?>
        </div>
    </div>
<?php 
elseif ( is_search() ) : ?>
    <div class="site-headlines parallax-container">
        <div class="parallax">
        <?php
        $page_id = get_queried_object_id();
        if ( has_post_thumbnail( $page_id ) ) :
            $image_array = wp_get_attachment_image_src( get_post_thumbnail_id( $page_id ), 'full' );
            $image = $image_array[0];
        else :
            $image = get_template_directory_uri() . '/assets/default-background.png';
        endif;
        echo '<img src="' . $image . '">'; 
        ?>
        </div>
        <div class="headline-entry">
            <h1 class="headline">
            <?php
            if ( is_category(20) ) :
                printf( esc_html__( '検索結果: %s', 'pripress' ), '<span>' . get_search_query() . '</span>' );
            else : 
                printf( esc_html__( 'Search Result: %s', 'pripress' ), '<span>' . get_search_query() . '</span>' );
            endif; ?>
            </h1>
        </div>
    </div>
<?php 
else : ?>
    <div class="site-headlines parallax-container">
        <div class="parallax">
        <?php
        $page_id = get_queried_object_id();
        if ( has_post_thumbnail( $page_id ) ) :
            $image_array = wp_get_attachment_image_src( get_post_thumbnail_id( $page_id ), 'full' );
            $image = $image_array[0];
        else :
            $image = get_template_directory_uri() . '/assets/default-background.png';
        endif;
        echo '<img src="' . $image . '">'; 
        ?>
        </div>
        <div class="headline-entry">
            <h1 class="headline"><?php wp_title( '', true ); ?></h1>
        </div>
    </div>
<?php
endif; ?>

