<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pripress
 */

?>

				<div class="section">
					<div class="works-cluster owl-carousel">
						<?php 
						$the_query = new WP_Query( array ('posts_per_page' => 10, 'cat' => 9 ) );
						while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
						<div class="item">		
							<?php 
							if ( has_post_thumbnail() ) : ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?></a>
							<?php 
							else : ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
								<div class="feat-img-pholder">
									<img src="<?php bloginfo('stylesheet_directory')?>/assets/logo-cebu_pripress-dark.png" />
								</div>
							</a>
							<?php 
							endif; ?>
							<p class="title"><?php the_title(); ?></p>
						</div>
						<?php 
						endwhile;
						wp_reset_postdata();
						?>
					</div>
				</div>