<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pripress
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="google" value="notranslate">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/favicon-16x16.png">
	<link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/favicon.ico" type="image/x-icon">
	<link rel="manifest" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class('background grey-200'); ?>>
<div id="page" class="site background grey-50">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'pripress' ); ?></a>

	<header id="masthead" class="site-header">
		<div id="branding-upper" class="site-branding-upper background grey-200 row">
			<div class="site-logo">
			<?php 
			if ( has_custom_logo() ): the_custom_logo(); 
			else : ?>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/logo-cebu_pripress_center.png"></a>
			<?php 
			endif; 
			?>
			</div>
			<div class="button-cluster">
				<a class="waves-effect waves-light btn" href="tel:+63325170694"><i class="font-icons left">&#xE0B0;</i>(63)32-5170694</a>
				<a class="waves-effect waves-light btn grey-800" href="<?php echo esc_url( get_permalink(28) ); ?>"><i class="font-icons left">&#xE894;</i>Japanese</a>
			</div>
		</div><!-- .site-branding-upper -->
		<div id="branding-lower" class="site-branding-lower background white row">
			<div class="site-menu">
				<a id="menu" class="site-menu-button button-collapse" data-activates="mobile-nav" href="#"><i class="font-icons">&#xE5D2;</i></a>
			</div>
			<div class="site-title">
			<?php
			if ( is_front_page() ) : ?>
				<h1 class="site-title-text"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php 
			else : ?>
				<p class="site-title-text"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php wp_title( '', true ); ?>&nbsp;-&nbsp;<?php bloginfo( 'name' ); ?></a></p>
			<?php 
			endif; 
			?>
			</div><!-- .site-title -->
			<div class="site-nav">
				<nav id="main-navigation" class="site-navigation-l">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'english-menu',
				) );
				?>
				</nav><!-- #site-navigation -->
			</div><!-- .site-nav -->
			<div class="site-search">
				<a id="search-btn" class="site-search-button" href="#!"><i class="font-icons">&#xE8B6;</i></a>
			</div><!-- .site-search -->
			<?php get_template_part( 'searchform' ); ?>
		</div><!-- .site-branding-lower -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
	<?php get_template_part( 'template-parts/content', 'navigation' ); ?>
	
