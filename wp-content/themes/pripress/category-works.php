<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pripress
 */

get_header(); ?>

	<?php get_template_part( 'template-parts/content', 'atf' ); ?>

	<div id="primary" class="content-area has-sidebar">
		<main id="main" class="site-main">
			<div class="row">
			<?php
			if ( have_posts() ) : ?>

				<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/*
					* Include the Post-Format-specific template for the content.
					* If you want to override this in a child theme, then include a file
					* called content-___.php (where ___ is the Post Format name) and that will be used instead.
					*/
					get_template_part( 'template-parts/content-works-main', get_post_format() );

				endwhile;

				pripress_pagination();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif; ?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar('works');
get_footer();
