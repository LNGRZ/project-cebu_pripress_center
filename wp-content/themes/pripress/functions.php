<?php
/**
 * pripress functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package pripress
 */

if ( ! function_exists( 'pripress_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function pripress_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on pripress, use a find and replace
		 * to change 'pripress' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'pripress', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'English', 'pripress' ),
			'menu-2' => esc_html__( 'Japanese', 'pripress' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'pripress_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 65,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'pripress_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function pripress_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'pripress_content_width', 640 );
}
add_action( 'after_setup_theme', 'pripress_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function pripress_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'pripress' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'pripress' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'pripress_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function pripress_scripts() {
	wp_enqueue_style( 'pripress-style', get_stylesheet_uri() );

	wp_enqueue_style( 'pripress-style-base', get_stylesheet_directory_uri() . '/style-base.css', true );

	wp_enqueue_script( 'google-webfontloader', 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js', array(), '1.6.26', true );

	wp_enqueue_script( 'pripress-jquery', get_template_directory_uri() . '/js/jquery-3.2.1.min.js', array(), '20171215', true );

	wp_enqueue_script( 'pripress-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'pripress-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'pripress-objectfit', get_template_directory_uri() . '/js/object-fit.js', array(), '20171215', true );

	wp_enqueue_script( 'pripress-framework', get_template_directory_uri() . '/js/framework.js', array(), '20171215', true );

	wp_enqueue_script( 'pripress-initialize', get_template_directory_uri() . '/js/init.js', array(), '20171215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'pripress_scripts' );

/**
 * Add active class to navigation
 */
 add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
 
 function special_nav_class ($classes, $item) {
	 if (in_array('current-menu-item', $classes) ){
		 $classes[] = 'active ';
	 }
	 return $classes;
 }
 
/**
 * Filter the except length to 25 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
 function pripress_custom_excerpt_length( $length ) {
	return 25;
 }
 add_filter( 'excerpt_length', 'pripress_custom_excerpt_length', 999 );
 
/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
 function pripress_excerpt_more( $more ) {
	return '...';
 }
 add_filter( 'excerpt_more', 'pripress_excerpt_more' );

/**
 * get_excerpt
 */
 function get_excerpt($count){
	$permalink = get_permalink($post->ID);
	$excerpt = get_the_content();
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, $count);
	$excerpt = $excerpt . '...';
	return $excerpt;
 }
 
/**
 * Filter categories (The post on this categories will not show on the blog page).
 */
 function exclude_category( $query ) {
	if ($query->is_home() && $query->is_main_query() ) {
		$query->set( 'cat', '22' );
	}
 }
 add_action( 'pre_get_posts', 'exclude_category' );

/**
 * Retrieve path of category-japanese-articles.php template for 'Japanese Articles' subcategories
 * Filtered via 'category_template' hook
 *
 * @param string Full path to category template file.
 *
 * @return string Full path to category template file.
 */
 function get_category_japaneseArticles_template( $template ) {
	$category_ID = intval( get_query_var('cat') );
	$category = get_category( $category_ID );
	$parent_ID = $category->parent;
	$parent_category = get_category( $parent_ID );

	if ( !is_wp_error( $parent_category ) AND $parent_category->slug == 'japanese-articles' ) {
		if ( file_exists( TEMPLATEPATH . '/category-' . $parent_category->slug . '.php' )) {
		return TEMPLATEPATH . '/category-' . $parent_category->slug . '.php';
		}
	} else {
		return $template;
	}
 }
 add_filter( 'category_template', 'get_category_japaneseArticles_template' );

/**
 * Exclude Categories for Users (exept administrator)
 */
 add_filter('list_terms_exclusions', 'pripress_list_terms_exclusions', 10, 2);
 function pripress_list_terms_exclusions( $exclusions, $args ) {
	global $pagenow;
	if (in_array($pagenow,array('post.php','post-new.php')) && 
	!current_user_can('manage_options')) {
	$exclusions = " {$exclusions} AND t.slug NOT IN ('website-contents','english-articles','japanese-articles','pripress-works')";
	}
	return $exclusions;
 }

/**
 * Adds Extra Information on user profile.
 */
 function pripress_extra_profile_fields( $user ) { ?>

 <h3>User Extra Information</h3>

 <table class="form-table">
	<tr>
		<th><label for="jobtitle">Job Title</label></th>
		<td>
			<input type="text" name="jobtitle" id="jobtitle" value="<?php echo esc_attr( get_the_author_meta( 'jobtitle', $user->ID ) ); ?>" class="regular-text" /><br />
			<span class="description">Please enter your job title. (e.g. Graphic Designer, Web Designer, Web Developer, etc... )</span>
		</td>
	</tr>
 </table>

 <?php }

 add_action( 'show_user_profile', 'pripress_extra_profile_fields' );
 add_action( 'edit_user_profile', 'pripress_extra_profile_fields' );

 function my_save_extra_profile_fields( $user_id ) {
 if ( !current_user_can( 'edit_user', $user_id ) )
 	return false;
 update_usermeta( $user_id, 'jobtitle', $_POST['jobtitle'] );
 }

 add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
 add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

/**
 * Custom Search Queries
 * Allows the use of multiple hidden filters on search.php
 */
 function SearchFilter($query) {
	$cat = $_GET['cat'];
	if (!$cat) {
		$cat = 'any';
	}
	if ($query->is_search) {
		$query->set('cat', $cat);
	};
	return $query;
 }

 add_filter('pre_get_posts','SearchFilter');

/**
 * Pagination
 */
 function pripress_pagination() {
	if( is_singular() )
		return;
	global $wp_query;
	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;
	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );
	/** Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;
	/** Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}
	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}
	echo '<div class="page-navi"><ul class="pagination">' . "\n";
	/** Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li class="waves-effect">%s</li>' . "\n", get_previous_posts_link('<i class="font-icons">&#xE314;</i>') );
	else
		printf( '<li class="disabled"><i class="font-icons">&#xE314;</i></li>' );
	/** Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="waves-effect active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}
	/** Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="waves-effect active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}
	/** Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="waves-effect active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}
	/** Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li class="waves-effect">%s</li>' . "\n", get_next_posts_link('<i class="font-icons">&#xE315;</i>') );
	else
		printf( '<li class="disabled"><i class="font-icons">&#xE315;</i></li>' );

	echo '</ul></div>' . "\n";
 }

/**
 * Pripress Logo
 */
 function pripress_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
        background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/logo-cebu_pripress_center.png);
		height:45px;
		width:100px;
		background-size: 100px;
		background-repeat: no-repeat;
        padding-bottom: 10px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'pripress_login_logo' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}