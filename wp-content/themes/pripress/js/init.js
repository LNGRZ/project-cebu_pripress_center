( function( $ ) {
$(function(){

  $('.button-collapse').sideNav();
  $('.parallax').parallax();

}); // end of document ready
} )( jQuery ) ; // end of jQuery name space

//Initialization
$(document).ready(function(){
  $('.collapsible').collapsible();
  $('ul.tabs').tabs();
});


//Scroll Then Fixed
var brand_nav = $("#branding-lower");
var sideFocus1 = $("#side-focus-1");
var sideFocus2 = $("#side-focus-2");
var sideFocus3 = $("#side-focus-3");
var floatDialer = $("#float-dial");
$(window).scroll(function() {    
  var scroll = $(window).scrollTop();
    if (scroll >= newFunction()) {
      brand_nav.addClass("trans-fixed");
    } 
    else {
      brand_nav.removeClass("trans-fixed");
    }
    if (scroll >= newFunction_1()) {
      sideFocus1.addClass("fixed");
    } 
    else {
      sideFocus1.removeClass("fixed");
    }

    if (scroll >= newFunction_2()) {
      sideFocus2.addClass("fixed");
    } 
    else {
      sideFocus2.removeClass("fixed");
    }

    if (scroll >= newFunction_3()) {
      sideFocus3.addClass("fixed");
    } 
    else {
      sideFocus3.removeClass("fixed");
    }

    if (scroll >= newFunction_4()) {
      floatDialer.addClass("show");
    } 
    else {
      floatDialer.removeClass("show");
    }
});

function newFunction() {
    return 65;
}

function newFunction_0() {
    return 700;
}

function newFunction_1() {
    return 900;
}

function newFunction_2() {
    return 380;
}

function newFunction_3() {
    return 480;
}

function newFunction_4() {
    return 65;
}

//Remove class "Show" (Time interval)
var $removeShow = $("#float-dial");
setInterval(function(){
  $removeShow.removeClass("show");
}, 5000);

//Margin-top when Sidebar reaches Footer
$(window).scroll(function () { 
  footertotop = ($('#colophon').position().top);
  scrolltop = $(document).scrollTop()+newFunction_0();
  difference = scrolltop-footertotop;
    if (scrolltop > footertotop) {
      $('.sidebar-focus.fixed').css('margin-top',  0-difference);
    } 
    else  {
      $('.sidebar-focus.fixed').css('margin-top', 0);
    }
});

//Remove header when scrolldown and show when scrollup
var lastScroll = 0;
jQuery(document).ready(function($) {
    $("#branding-lower").addClass("mobile");
    $(window).scroll(function(){
        setTimeout(function() {
            var scroll = $(window).scrollTop();
            if (scroll > lastScroll + 10) {
                $("#branding-lower").removeClass("mobile");
            } else if (scroll < lastScroll - 10) {
                $("#branding-lower").addClass("mobile");
            }
            lastScroll = scroll;
        }, 1000);
    });
});

//Add class show on click (used for search bar)
$(document).ready(function() {
  $('#search-btn').click(function() {
      $('#header-search').addClass('show');
  });
});
//Remove class show on click (used for search bar)
$(document).ready(function() {
  $('#cancel-btn').click(function() {
      $('#header-search').removeClass('show');
  });
});


//Owl Carousel
$(document).ready(function() {
  $('.owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      601: {
        items: 3,
        nav: true
      },
      993: {
        items: 4,
        nav: true,
        loop: false,
        margin: 20
      },
      1300: {
        items: 5,
        nav: true,
        loop: false,
        margin: 20
      }
    }
  })
})

//FOUT fix using Webfont Loader
WebFont.load({
  google: {
  families: ['Noto Sans JP', 'Font Icons']
  }
});