<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pripress
 */
 
?>

		<div class="recent-aritcles">
			<h5 class="main-title">Recent Articles</h5>
			<div class="card">
				<div class="card-content">
					<?php 
					$the_query = new WP_Query( 'posts_per_page=5&cat=1,2,3,10,11,12' );
					while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
					<div class="feat-entry-cluster row">
						<div class="feat-img-s">
							<?php 
							if ( has_post_thumbnail() ) : ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('small'); ?></a>
							<?php 
							else : ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
								<div class="feat-img-pholder-s">
									<img src="<?php bloginfo('stylesheet_directory')?>/assets/logo-cebu_pripress-dark.png" />
								</div>
							</a>
							<?php 
							endif; ?>
						</div>
						<div class="feat-entry-s">
							<p class="title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></p>
						</div>
					</div>
					<?php 
					endwhile;
					wp_reset_postdata();
					?>	
				</div>
			</div>
		</div>
