<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pripress
 */

get_header('jp'); ?>

	<?php get_template_part( 'template-parts/content', 'atf' ); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section>
				
				<ul class="tabs tabs-fixed-width">
					<li class="tab col s4"><a href="#tab1">Company</a></li>
					<li class="tab col s4"><a href="#tab2">Staff</a></li>
					<li class="tab col s4"><a href="#tab3">Others</a></li>
				</ul>

				<div id="tab1">
					<div class="title-bar">
						<h2 class="title">株式会社 プリプレス・センター</h2>
					</div>
					<div class="section">
						<div class="card row">
							<div class="col s12 l8">
								<div class="card-content">
									<?php
									while ( have_posts() ) : the_post();

										get_template_part( 'template-parts/content', 'page' );

										// If comments are open or we have at least one comment, load up the comment template.
										if ( comments_open() || get_comments_number() ) :
											comments_template();
										endif;

									endwhile; // End of the loop.
									?>
									<table class="about-com-table">
										<tr><td>会社名</td><td><?php echo get_post_meta($post->ID, 'CompanyName', true); ?></td></tr>
										<tr><td>本社</td><td><?php echo get_post_meta($post->ID, 'Address', true); ?></td></tr>
										<tr><td>創立</td><td><?php echo get_post_meta($post->ID, 'Founding', true); ?></td></tr>
										<tr><td>最高経営責任者（CEO）</td><td><?php echo get_post_meta($post->ID, 'CEOName', true); ?></td></tr>
										<tr><td>ゼネラルマネージャー</td><td><?php echo get_post_meta($post->ID, 'ManagerName', true); ?></td></tr>
									</table>
								</div>
							</div>
							<div class="col s12 l4">
								<div class="card-content">
									
									<div class="about-logo-container">
									<img src="<?=bloginfo('stylesheet_directory')?>/assets/logo-cebu_pripress-dark.png" />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="title-bar">
						<h2 class="title">短いクリップ</h2>
					</div>
					<div class="short-clip">
						<div classs="section">
							<p class="no-vid">No video at the moment...</p>
						</div>
					</div>
					<div class="title-bar">
						<h2 class="title">メッセージ</h2>
					</div>
					<div class="section">
						<div class="card row">
							<div class="col s12 m7 l8">
								<div class="card-content">
									<?php
									$post_id = 174;
									$queried_post = get_post($post_id);
									$title = $queried_post->post_title;
									?>
									<h3 class="title"><?php echo $title;?></h3>
									<p class="entry"><?php echo $queried_post->post_content; ?></p>
								</div>
							</div>
							<div class="col s12 m5 l4">
								<div class="card-content">
									<?php echo get_the_post_thumbnail( $post_id, 'img-x-center', 'post_thumbnail') ?>
								</div>
							</div>
						</div>
					</div>
					<div class="section no-pad policy-slogan">
						<div class="card background blue">
							<div class="card-content row">
								<?php
								$queried_post_1 = get_post(240);
								$queried_post_2 = get_post(242);
								$title_1 = $queried_post_1->post_title;
								$title_2 = $queried_post_2->post_title;
								?>
								<h3 class="title"><?php echo $title_1;?>: <span class="entry"><?php echo $queried_post_1->post_content; ?></span></h3>
								<h3 class="title"><?php echo $title_2;?>: <span class="entry"><?php echo $queried_post_2->post_content; ?></span></h3>
							</div>
						</div>
					</div>
					<div class="title-bar">
						<h2 class="title">ロケーション</h2>
					</div>
					<div class="section">
					<?php get_template_part( 'template-parts/content', 'map' ); ?>
					</div>				
				</div>

				<div id="tab2">
					<div class="title-bar">
						<h2 class="title">会社スタッフ</h2>
					</div>
					<div class="section">
						<div class="card no-mab">
							<div class="card-content no-pad">
								<div class="about-staff">
									<div class="staff-cluster">
										<div class="cluster row">
										<?php
										$blogusers = get_users( 'blog_id=1&orderby=nicename&role__not_in=Administrator&' );
										foreach ( $blogusers as $user ) {
										if ( $user ) : ?>
											<div class="staff-entry">
												<img src="<?php echo esc_url( get_avatar_url( $user->ID ) ); ?>" />
												<span class="name"><?php echo esc_html( $user->display_name ); ?></span>
												<span class="job"><?php echo esc_html( $user->jobtitle ); ?></span>
											</div>
										<?php 
										endif; } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div id="tab3" class="about-others">
					<div class="title-bar">
						<h2 class="title">パートナー企業</h2>
					</div>
					<div class="section">
						<div class="card background blue-50 row">
							<div class="col s12 l8">
								<div class="card-content">
									<?php
									$post_id = 187;
									$queried_post = get_post($post_id);
									$title = $queried_post->post_title;
									?>
									<h3 class="title"><?php echo $title;?></h3>
									<p class="entry"><?php echo $queried_post->post_content; ?></p>
									<table class="about-com-table">
										<tr><td>会社名</td><td><?php echo get_post_meta($post->ID, 'CompanyName', true); ?></td></tr>
										<tr><td>本社</td><td><?php echo get_post_meta($post->ID, 'Address', true); ?></td></tr>
										<tr><td>創立</td><td><?php echo get_post_meta($post->ID, 'Founding', true); ?></td></tr>
										<tr><td>最高経営責任者（CEO）</td><td><?php echo get_post_meta($post->ID, 'CEOName', true); ?></td></tr>
										<tr><td>ゼネラルマネージャー</td><td><?php echo get_post_meta($post->ID, 'ManagerName', true); ?></td></tr>
									</table>
								</div>
							</div>
							<div class="col s12 m3 l4">
								<div class="card-content">
									<div class="about-logo-container">
									<?php echo get_the_post_thumbnail( $post_id, 'post_thumbnail') ?>
									</div>
								</div>
							</div>
						</div>
						<div class="card no-mab background green-50">
							<div class="card-content row">
								<div class="col s12 l8">
									<?php
									$post_id = 189;
									$queried_post = get_post($post_id);
									$title = $queried_post->post_title;
									?>
									<h3 class="title"><?php echo $title;?></h3>
									<p class="entry"><?php echo $queried_post->post_content; ?></p>
									<table class="about-com-table">
										<tr><td>C会社名</td><td><?php echo get_post_meta(189, 'CompanyName', true); ?></td></tr>
										<tr><td>札幌本店</td><td><?php echo get_post_meta(189, 'MainAddress1', true); ?></td></tr>
										<tr><td>東京本店</td><td><?php echo get_post_meta(189, 'MainAddress2', true); ?></td></tr>
										<tr><td>創立</td><td><?php echo get_post_meta(189, 'Founding', true); ?></td></tr>
										<tr><td>最高経営責任者（CEO）</td><td><?php echo get_post_meta(189, 'CEOName', true); ?></td></tr>
										<tr><td>取締役社長</td><td><?php echo get_post_meta(189, 'ManagerName', true); ?></td></tr>
									</table>
								</div>
								<div class="col s12 m3 l4">
									<div class="about-logo-container">
									<?php echo get_the_post_thumbnail( $post_id, 'post_thumbnail') ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer('jp');
