<div id="header-search" class="search-bar">
    <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
        <label>
            <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
            <input type="text" class="search-field"
                placeholder="<?php 
                            if( is_page( array(13,28,34,37,39,124) ) || is_category( array(7,13,14,15,16,17,18)) ):
                                echo esc_attr_x( 'サーチ', 'placeholder' ) ;
                            else :
                                echo esc_attr_x( 'Search', 'placeholder' ) ;
                            endif ?>"
                value="<?php echo get_search_query() ?>" name="s"
                title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
            <input type="hidden" name="cat" value="20" />
        </label>
        <input type="submit" class="search-submit font-icons" value="<?php echo esc_attr_x( '&#xE8B6;', 'submit button' ) ?>" />
    </form>
    <div class="site-cancel">
        <a id="cancel-btn" class="site-cancel-button" href="#!"><i class="font-icons">&#xE5CD;</i></a>
    </div>
</div>