<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package pripress
 */
if ( is_category(20) ) :
	get_header('jp'); 
else :
	get_header();
endif; 

get_template_part( 'template-parts/content', 'atf' ); ?>	

	<section id="primary" class="content-area has-sidebar">
		<main id="main" class="site-main">
			<div class="row">
			<?php
			if ( have_posts() ) :
				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/**
					* Run the loop for the search to output the results.
					* If you want to overload this in a child theme then include a file
					* called content-search.php and that will be used instead.
					*/
					get_template_part( 'template-parts/content', 'search' );

				endwhile;

				pripress_pagination();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif; ?>
			</div>
		</main><!-- #main -->
	</section><!-- #primary -->

<?php
if ( is_category(20) ) :
	get_sidebar('jp');
	get_footer('jp');
else :
	get_sidebar();
	get_footer();
endif; ?>

