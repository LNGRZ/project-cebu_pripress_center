<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pripress
 */

get_header('jp'); ?>

	<?php get_template_part( 'template-parts/content', 'atf' ); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section>

				<ul class="tabs tabs-fixed-width">
					<li class="tab col s4"><a href="#tab1">情報</a></li>
					<li class="tab col s4"><a href="#tab2">形態</a></li>
				</ul>
				<div id="tab1">
				<div class="title-bar">
					<h2 class="title">連絡先情報</h2>
				</div>
				<div class="section">
					<div class="card">
						<div class="card-content">
						<?php
						while ( have_posts() ) : the_post();

							get_template_part( 'template-parts/content', 'page' );

							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;

						endwhile; // End of the loop.
						?>
						</div>
					</div>
				</div>
				<div class="section row">
					<div class="contact-info">
						<div class="card background red">
							<div class="c_info-cluster">
								<i class="font-icons left">&#xE0CD;</i><p><?php echo get_post_meta(28, 'PNumber', true); ?></p>
								<i class="font-icons left">&#xE158;</i><p><?php echo get_post_meta(28, 'Email', true); ?></p>
								<i class="font-icons left">&#xE192;</i><p><?php echo get_post_meta(28, 'Sched', true); ?></p>
								<i class="font-icons left">&#xE0C8;</i><p><?php echo get_post_meta(28, 'Address', true); ?></p>
							</div>
						</div>
					</div>
					<div class="location-gmap background white">
					<?php get_template_part( 'template-parts/content', 'map' ); ?>
					</div>
				</div>
				</div>
				<div id="tab2">
					<div class="title-bar">
						<h2 class="title">お問い合わせフォーム</h2>
					</div>
					<div class="section">
						<div class="card row">
							<div class="col s12 l6">
								<div class="card-content">
								<?php echo do_shortcode('[contact-form-7 id="277" title="Contact-Page-jp"]') ;?>
								</div>
							</div>
							<div class="col s12 l6">
								<div class="card-content">
									<?php
									$post_id = 281;
									$queried_post = get_post($post_id);
									$title = $queried_post->post_title;
									?>
									<h3 class="title"><?php echo $title;?></h3>
									<p class="entry"><?php echo $queried_post->post_content; ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			<section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer('jp');
