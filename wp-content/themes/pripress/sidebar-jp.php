<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pripress
 */

?>

<div id="secondary" class="site-sidebar">
	<aside id="aside" class="site-aside">
	<?php 
	if ( is_category(20) ) :
		include( 'sidebar-a-jp.php' ); ?>
		<div id="side-focus-1" class="sidebar-focus">
		<?php include( 'sidebar-c-jp.php' ); ?>
		</div>

	<?php 
	elseif ( is_category() ) :
		include( 'sidebar-b-jp.php' ); ?>
		<div id="side-focus-1" class="sidebar-focus">
		<?php include( 'sidebar-c-jp.php' ); ?>
		</div>

	<?php
	else : 
		include( 'sidebar-a-jp.php' );
		include( 'sidebar-b-jp.php' );
		include( 'sidebar-c-jp.php' );
	endif 
	?>
	</aside>
</div><!-- #secondary -->
