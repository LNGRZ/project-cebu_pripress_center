<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pripress
 */

?>

	<div id="float-dial" class="floating-dialer fixed-action-btn">
		<a class="btn btn-floating btn-large" href="tel:+63325170694"><i class="font-icons left">&#xE0B0;</i></a>
	</div>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer background grey-300">
		<div class="section row">
			<div class="site-external">
				<div class="follow-us">
					<span class="label">Follow Us</span>
					<div class="s-media-cluster">
						<a href="https://plus.google.com" target="_blank"><img src="<?=bloginfo('stylesheet_directory')?>/assets/icon-gplus.png" alt="Google Plus" /></a>
						<a href="https://facebook.com" target="_blank"><img src="<?=bloginfo('stylesheet_directory')?>/assets/icon-facebook.png" alt="Facebook" /></a>
						<a href="https://twitter.com" target="_blank"><img src="<?=bloginfo('stylesheet_directory')?>/assets/icon-twitter.png" alt="Twitter" /></a>
					</div>
				</div>
				<div class="partner-com">
					<span class="label">Partners</span>
					<div class="s-partners-cluster row">
						<a href="#" target="_blank"><img src="<?=bloginfo('stylesheet_directory')?>/assets/logo-pripress.png" alt="Pripress Center Inc" /></a>
						<a href="#" target="_blank"><img src="<?=bloginfo('stylesheet_directory')?>/assets/logo-dmc.png" alt="DMC" /></a>
						<a href="#" target="_blank"><img src="<?=bloginfo('stylesheet_directory')?>/assets/logo-shigeisha.png" alt="Shigeisha" /></a>
					</div>
				</div>
			</div>
		</div>
		<div class="section">
			<div class="site-info">
				<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?=bloginfo('stylesheet_directory')?>/assets/logo-cebu_pripress-footer.png" alt="<?php bloginfo( 'name' ); ?>" /></a>
				<a class="site-name" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">&copy; <?php echo get_the_date('Y'); ?>&nbsp;<?php bloginfo( 'name' ); ?></a>
			</div><!-- .site-info -->
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
